package gredis

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestClient(t *testing.T) {
	assert := assert.New(t)
	_, ok := Client("cache")
	assert.True(ok)
	_, ok = Client("gredis")
	assert.False(ok)
}
