package gredis

import (
	"context"
	"log"
	"os"
	"path/filepath"
	"sync"
	"time"

	"github.com/hotmall/commandline"
	"github.com/redis/go-redis/v9"
)

var (
	once    sync.Once
	clients = make(map[string]redis.UniversalClient)
	m       sync.RWMutex
)

func init() {
	once.Do(func() {
		initClient(commandline.PrefixPath())
	})
}

func initClient(prefix string) {
	optionFile := filepath.Join(prefix, "etc", "conf", "gredis.json")
	if !fileExist(optionFile) {
		log.Fatalf("[gredis] config file is not exist, file = %s\n", optionFile)
	}
	options, err := loadOptions(optionFile)
	if err != nil {
		log.Fatalf("[gredis] load config file(%s) fail, err = %s\n", optionFile, err.Error())
	}

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	for name, option := range options {
		c := redis.NewUniversalClient(&redis.UniversalOptions{
			Addrs:          option.Addrs,
			DB:             option.DB,
			PoolSize:       option.PoolSize,
			MinIdleConns:   option.MinIdleConns,
			ReadOnly:       option.ReadOnly,
			RouteByLatency: option.RouteByLatency,
			RouteRandomly:  option.RouteRandomly,
			MasterName:     option.MasterName,
			Username:       option.Username,
			Password:       option.Password,
		})

		if _, err := c.Ping(ctx).Result(); err != nil {
			log.Printf("[gredis] the ping result was not ok, err = %s\n", err)
			continue
		}

		addOne(name, c)
	}
}

func fileExist(file string) bool {
	_, err := os.Stat(file)
	if err != nil && os.IsNotExist(err) {
		return false
	}
	return true
}

// Client return redis.UniversalClient instance by name
func Client(name string) (c redis.UniversalClient, ok bool) {
	m.RLock()
	defer m.RUnlock()
	c, ok = clients[name]
	return
}

// Close close all clients
func Close() {
	m.Lock()
	defer m.Unlock()

	for name, c := range clients {
		c.Close()
		delete(clients, name)
	}
}

func addOne(name string, client redis.UniversalClient) {
	m.Lock()
	defer m.Unlock()

	if c, ok := clients[name]; ok {
		c.Close()
		delete(clients, name)
	}
	clients[name] = client
}
