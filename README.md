# gredis

## Client json config example

${Prefix}/etc/conf/gredis.json

```json
{
    "cache": {
        "addrs": ["localhost:6379", "localhost:6380"],
        "db": 0,
        "username": "",
        "password": "",
        "poolSize": 10,
        "minIdleConns": 1,
        "readOnly": false,
        "routeByLatency": true,
        "routeRandomly": false,
        "masterName": ""
    }
}
```

当 addrs 只有一个 item 时，返回的是simple client，如果是多个 item 时返回的是 cluster client，当 masterName 不为 “” 时，返回的是 failover client

// Only cluster clients.
ReadOnly       bool
RouteByLatency bool
RouteRandomly  bool

// The sentinel master name. Only failover clients.
MasterName string
