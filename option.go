package gredis

import (
	"encoding/json"
	"os"
)

type option struct {
	// Either a single address or a seed list of host:port addresses
	// of cluster/sentinel nodes.
	Addrs          []string `json:"addrs"`
	DB             int      `json:"db"` // Only single-node and failover clients.
	Username       string   `json:"username"`
	Password       string   `json:"password"`
	PoolSize       int      `json:"poolSizee"`
	MinIdleConns   int      `json:"minIdleConns"`
	ReadOnly       bool     `json:"readOnly"`
	RouteByLatency bool     `json:"routeByLatency"`
	RouteRandomly  bool     `json:"routeRandomly"`
	MasterName     string   `json:"masterName"`
}

type options map[string]option

func loadOptions(file string) (o options, err error) {
	bytes, err := os.ReadFile(file)
	if err != nil {
		return
	}
	if err = json.Unmarshal(bytes, &o); err != nil {
		return
	}
	return
}
